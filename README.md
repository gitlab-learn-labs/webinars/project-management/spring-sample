# Demo Project for Jira Integration with GitLab

👉 **IMPORTANT** 👈

This project is meant as a basis for integrating Jira with GitLab. The Jira cloud instance that it's connected to has limited users, so if you fork, access to this specific instance is not guaranteed.

See [documentation](https://docs.google.com/document/d/1ZWny0RYY01DXZ1J_nLmTg9QwNJoS8LmE_WTQ8x4xWtY/edit) (internal) for a high level walkthrough of how to setup your own Jira Cloud instance.

## Warning

__‼️  This project is still vulnerable with Spring4Shell CVE-2022-22965. Do NOT deploy this project to a live environment ‼️__

## How It Works

There are many ways to integrate Jira with GitLab, and this project covers disabling the built-in `Issues` module within GitLab and replacing it with read-only iFrame access to the connected Jira instance. You can interact with the Jira issues by referencing them in commit messages and comments on MRs, and they are automatically closed when using the proper keywords (`CLOSES, RESOLVES, FIXES`) in comments or descriptions. You can also create branches directly in Jira to auto-populate the branch name with the Jira issue ID

To sync commits with the Jira issue, commit messages should reference the Jira issue ID

## Configuration Details

### Instance URL

The Jira instance connected can be found [here](https://eaglefang.atlassian.net)

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring)

### CI/CD with Auto DevOps `includes:`

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

This project uses a few security scanning jobs via `includes:` in the `.gitlab-ci.yml`

The `deploy` job is spoofed to show `environments` usage

### Jira Integration

Users with the `Maintainer` role can see the integration settings under `Settings > Integrations`
